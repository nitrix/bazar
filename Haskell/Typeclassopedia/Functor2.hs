import Data.Functor

newtype Tuple a b = MkTuple (a, b)
data Pair a = MkPair a a

instance Functor (Tuple a) where
    fmap f (MkTuple (x, y)) = MkTuple (x, f y)

instance Functor Pair where
    fmap f (MkPair x y) = MkPair (f x) (f y)
