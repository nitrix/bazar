data ITree a = Leaf (Int -> a) | Node [ITree a]

instance Functor ITree where
    fmap f (Leaf g) = Leaf $ f . g
    fmap _ (Node []) = Node $ []
    fmap f (Node (x:xs)) = Node $ fmap f x : map (fmap f) xs
