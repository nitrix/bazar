import Data.Either
import Data.Functor

newtype MyEither a b = MkMyEither (Either a b)
newtype MyFunction r a = MkMyFunction (r -> a)

{-
class Functor (f :: * -> *) where
    fmap :: (a -> b) -> f a -> f b

`Either` has kind :: * -> * -> *
`Either e` has kind :: * -> *
 
The left part cannot be made accessible as a Functor, but it'd be possible as a Bifunctor.
Alternatively, it's possible to use some `newtype Flip f a b = Flip { unFlip = f b a }` type.
-}

instance Functor (MyEither e) where
    fmap f (MkMyEither (Right a)) = MkMyEither $ Right $ f a
    fmap f (MkMyEither (Left e)) = MkMyEither $ Left e

instance Functor (MyFunction r) where
    --fmap f (MkMyFunction g) = MkMyFunction $ f . g
    fmap f (MkMyFunction g) = MkMyFunction $ \x -> f $ g x
